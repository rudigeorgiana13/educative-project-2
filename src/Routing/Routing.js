import React from 'react'
import {Routes,Route} from 'react-router-dom'
import {Home} from '../pages/Home/index'
import {About} from "../pages/About";
import {EnrolNow} from "../pages/EnrolNow";
import {OurWork} from "../pages/OurWork";
import {Services} from "../pages/Services";


export const Routing = () => {
    return (
    <Routes>
        <Route path='/' element={<Home/>}> Home </Route>
        <Route path='/about' element={<About/> }> About </Route>
        <Route path='/enrol' element={<EnrolNow/> }> Enrol Now</Route>
        <Route path='/ourWork' element={<OurWork/> }> Our Work </Route>
        <Route path='/services' element={<Services/> }> Services </Route>
    </Routes>
     )
}