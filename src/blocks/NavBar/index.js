import React from "react";
import styled from "styled-components";
import {Link} from 'react-router-dom'



export const Main = styled.div`
  padding: 42px 116px;
  display: flex;
  flex-direction: row;
  
`;

export const LinksWrapper = styled.div`
  
`

export const Logo = styled.div`
  
  p {

  }
`


export const NavBar = () => {
  return (
    <Main>
      <LinksWrapper>
          <Link to='/'> Home </Link>
          <Link to='/about'> About </Link>
          <Link to='/enrol'> Enrol Now </Link>
          <Link to='/ourWork'> Our Work </Link>
          <Link to='/services'> Services </Link>
      </LinksWrapper>

        <Logo>
          <p> GB </p>
        </Logo>
    </Main>

  )
}